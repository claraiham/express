// importe le module Express
const express = require('express');
const app = express();
// on utilisera le port 3000 pour accéder au serveur
const port = 3000;


// définition d'une route GET sur l'URL racine ('/')
app.get('/', (req, res) => {
  // en arrivant sur localhost:3000/ le serveur nous affiche l'objet suivant :
  res.send({ name: 'Pikachu', power: 20, life: 50 }); // la variable res reçoit mon objet et renvoie une réponse HTTP
})

app.get('/login', (req, res) => { // après le slash dans le lie du navigateur ajouter login pour accder à l'objet suivant : 
    res.send({ email: 'patatedu31@yahoo.fr', password:"potatoescherimiam" }); // la variable res reçoit mon objet et renvoie une réponse HTTP
  })

app.get('/enigme', (req, res) => { //après le slash dans le lie du navigateur ajouter login pour accder à l'objet suivant : 
    res.send({ enigme1: 'Deux marins et leur capitaine sont dans un bateau. Les marins on la tête coupée, le capitaine est décapité. Combien de gens sont morts ? Pourquoi ?' }); // la variable res reçoit mon objet et renvoie une réponse HTTP
  })

// démarrage du serveur sur le port défini
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
})
